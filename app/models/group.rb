class Group < ApplicationRecord
  has_many :memberships
  has_many :users, through: :memberships
  has_many :roles
  has_many :stores
  has_many :products
end
