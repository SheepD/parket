class Purchase < ApplicationRecord
  belongs_to :store
  belongs_to :user
  has_many :purchase_items
  has_many :products, through: :purhcase_items
end
