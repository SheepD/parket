class Store < ApplicationRecord
  belongs_to :group
  has_many :store_memberships
  has_many :users, through: :store_memberships
  has_many :inventories
  has_many :products, through: :inventories
  has_many :orders
end
