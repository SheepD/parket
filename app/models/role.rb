class Role < ApplicationRecord
  belongs_to :group
  has_many :memberships
  has_many :users, through: :memberships

  OWNER = 'owner'.freeze
  ADMIN = 'admin'.freeze
  USER = 'user'.freeze
  VALID_ROLES = [
    OWNER,
    ADMIN,
    USER
  ].freeze
end
