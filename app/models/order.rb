class Order < ApplicationRecord
  belongs_to :store
  belongs_to :user
  has_many :order_items
  has_many :inventories, through: :order_items

  def products; end
end
