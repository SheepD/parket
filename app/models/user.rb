class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :confirmable, :lockable, :timeoutable, :trackable

  has_many :memberships
  has_many :roles, through: :memberships
  has_many :groups, through: :memberships
  has_many :store_memberships
  has_many :stores, through: :store_memberships
  has_many :purchases
  has_many :orders
end
