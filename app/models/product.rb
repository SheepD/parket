class Product < ApplicationRecord
  belongs_to :group
  has_many :inventories
  has_many :stores, through: :inventories
  has_many :purchase_items
  has_many :purchases, through: :purchase_items
end
