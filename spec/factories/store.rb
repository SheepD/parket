FactoryBot.define do
  factory :store do
    name { Faker::Company.name }
    location { Faker::Address.full_address }
    description { Faker::Lorem.paragraphs }
    group
  end
end
