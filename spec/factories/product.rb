FactoryBot.define do
  factory :product do
    group
    name { Faker::Commerce.product_name }
    description { Faker::Lorem.sentence }
    unit { 'kg' }
  end
end
