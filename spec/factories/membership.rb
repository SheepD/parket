FactoryBot.define do
  factory :membership do
    user
    group
    role
  end
end
