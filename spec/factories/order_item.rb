FactoryBot.define do
  factory :order_item do
    order
    inventory
    quantity { rand(1..100) }
    price { Faker::Commerce.price }
  end
end
