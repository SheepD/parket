FactoryBot.define do
  factory :order do
    user
    store
    order_timestamp { Time.now }
  end
end
