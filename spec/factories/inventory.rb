FactoryBot.define do
  factory :inventory do
    product
    store
    quantity { rand(1..100) }
    sale_price { Faker::Commerce.price }
    last_purchase_price { Faker::Commerce.price }
  end
end
