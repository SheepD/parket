FactoryBot.define do
  factory :purchase do
    store
    user
    purchase_date { Time.now }
  end
end
