FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    password { 'Parket#1' }
    password_confirmation { 'Parket#1' }
    confirmed_at { Time.now }

    factory :group_owner do
      transient do
        group { create :group }
      end

      after(:create) do |user, evaluator|
        group = evaluator.group
        role = create :role, name: Role::OWNER, group: group
        create :membership, user: user, group: group, role: role
      end
    end
  end
end
