FactoryBot.define do
  factory :purchase_item do
    purchase
    product
    quantity { rand(1..100) }
    price { Faker::Commerce.price }
  end
end
