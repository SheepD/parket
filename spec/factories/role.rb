FactoryBot.define do
  factory :role do
    name { Role::VALID_ROLES.sample }
    group
  end
end
