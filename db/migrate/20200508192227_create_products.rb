class CreateProducts < ActiveRecord::Migration[6.0]
  def change
    create_table :products do |t|
      t.references :group, null: false, foreign_key: true, index: true
      t.string :name
      t.text :description
      t.string :unit
      t.timestamps
    end
  end
end
