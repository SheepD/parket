class CreateStores < ActiveRecord::Migration[6.0]
  def change
    create_table :stores do |t|
      t.references :group, null: false, foreign_key: true, index: true
      t.string :name
      t.string :location
      t.text :description
      t.timestamps
    end
  end
end
