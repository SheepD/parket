class CreateOrderItems < ActiveRecord::Migration[6.0]
  def change
    create_table :order_items do |t|
      t.references :order, null: false, foreign_key: true, index: true
      t.references :inventory, null: false, foreign_key: true, index: true
      t.decimal :quantity
      t.decimal :price
      t.timestamps
    end
  end
end
