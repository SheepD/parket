class CreateRoles < ActiveRecord::Migration[6.0]
  def change
    create_table :roles do |t|
      t.string :name
      t.references :group, null: false, foreign_key: true, index: true
      t.timestamps
    end
  end
end
