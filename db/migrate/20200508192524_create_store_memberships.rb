class CreateStoreMemberships < ActiveRecord::Migration[6.0]
  def change
    create_table :store_memberships do |t|
      t.references :store, null: false, foreign_key: true, index: true
      t.references :user, null: false, foreign_key: true, index: true
      t.timestamps
    end
  end
end
