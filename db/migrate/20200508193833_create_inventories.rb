class CreateInventories < ActiveRecord::Migration[6.0]
  def change
    create_table :inventories do |t|
      t.references :product, null: false, foreign_key: true, index: true
      t.references :store, null: false, foreign_key: true, index: true
      t.decimal :quantity
      t.decimal :sale_price
      t.decimal :last_purchase_price
      t.timestamps
    end
  end
end
