class CreatePurchaseItems < ActiveRecord::Migration[6.0]
  def change
    create_table :purchase_items do |t|
      t.references :purchase, null: false, foreign_key: true, index: true
      t.references :product, null: false, foreign_key: true, index: true
      t.decimal :quantity
      t.decimal :price
      t.timestamps
    end
  end
end
